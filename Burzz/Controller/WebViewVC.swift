//
//  WebViewVC.swift
//  Burzz
//
//  Created by MAC MINI on 29/07/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
//import MBProgressHUD

class WebViewVC: UIViewController{
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        connectInstagram()
        
    }
    
    
    func connectInstagram()  {
        
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [API.INSTAGRAM_AUTHURL,API.INSTAGRAM_CLIENT_ID,API.INSTAGRAM_REDIRECT_URI, API.INSTAGRAM_SCOPE])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        webView.navigationDelegate = self
        webView.load(urlRequest)
        
    }
    
    
    //    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
    //        let requestURLString = (request.url?.absoluteString)! as String
    //        if requestURLString.hasPrefix(API.INSTAGRAM_REDIRECT_URI) {
    //            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
    //            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
    //            return false;
    //        }
    //        return true
    //    }
    //
    //    func handleAuth(authToken: String) {
    //        print("Instagram authentication token ==", authToken)
    //    }
    
    
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        let requestURLString = (request.url?.absoluteString)! as String
        if requestURLString.hasPrefix(API.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    func handleAuth(authToken: String) {
        API.INSTAGRAM_ACCESS_TOKEN = authToken
        print("Instagram authentication token ==", authToken)
        getUserInfo(){(data) in
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func getUserInfo(completion: @escaping ((_ data: Bool) -> Void)){
        let url = String(format: "%@%@", arguments: [API.INSTAGRAM_USER_POST,API.INSTAGRAM_ACCESS_TOKEN])
        var request = URLRequest(url: URL(string: url)!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            
            guard error == nil else {
                completion(false)
                //failure
                return
            }
            // make sure we got data
            guard let responseData = data else {
                completion(false)
                //Error: did not receive data
                return
            }
            do {
                
                let userData = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: AnyObject]
                
                if userData?.count != 0{
                    let dat = userData!["data"] as! NSArray
                    
                    if dat.count != 0 {
                        var InstaImageString = ""
                        for i in 0...dat.count{
                            let imagesDict = dat[i] as! NSDictionary
                            let images = imagesDict["images"] as! NSDictionary
                            let user = imagesDict["user"] as! NSDictionary
                            let instaName = user["username"] as! String
                            let ImageSubDict = images["low_resolution"] as! NSDictionary
                            let instaImgArr = ImageSubDict["url"] as! String
                            InstaImageArray.append(instaImgArr)
                            InstaImageString.append(instaImgArr)
                            
                            if i == 5{
                                let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KUPDATEPROFILE)")!

                                let dict : [String:String] = ["id":(currentUser.result?.id)!,"insta_images" : InstaImageString,"instagram":instaName]
                                
                                DispatchQueue.main.async {
                                    MBProgressHUD.showAdded(to: self.view, animated: true)
                                    self.CallAPIProfile(APIurl: url, dictData: dict )
                                }
                                break
                            }else{
                                InstaImageString.append(",")
                            }
                        }
                    }
                    completion(true)
                }else{
                    completion(false)
                }
            } catch let err {
                _ = err
                completion(false)
                //failure
            }
        })
        task.resume()
    }
    
    
    
    @IBAction func click_to_back(_ sender : Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
//*****************************************************************************
extension WebViewVC: WKNavigationDelegate{
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        if checkRequestForCallbackURL(request: navigationAction.request){
            decisionHandler(.allow)
        }
        else{
            decisionHandler(.cancel)
        }
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
           completionHandler(.performDefaultHandling,nil)
       }
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
           decisionHandler(.allow)
       }
}


extension WebViewVC{
    
    func CallAPIProfile(APIurl: URL,dictData : [String:String]?){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        let dict = (response.result.value as! NSDictionary)
                                        QLSharedPreference.sharedInstance.saveUserData(dict)
                                        currentUser = User1.init(dictionary:JSON)
                                        
                                        DispatchQueue.main.async {
                                            let alert = UIAlertController.init(title: "Instagram", message: "profile images fetched successfully", preferredStyle: .alert)
                                            let ok = UIAlertAction.init(title: "OK", style: .default) { (action) in
                                                self.navigationController?.popViewController(animated: true)
                                            }
                                            alert.addAction(ok)
                                            self.present(alert , animated: true)
                                        }
                                        
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
    
}

var InstaImageArray : [String] = []
