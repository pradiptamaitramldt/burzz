//
//  OTPVerifyVC.swift
//  Burzz
//
//  Created by Raul Menendez on 07/05/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
//import MBProgressHUD
import Alamofire
class OTPVerifyVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var txtotpOne: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOptTwo: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOtpthree: SkyFloatingLabelTextField!
    @IBOutlet weak var txtOtpFour: SkyFloatingLabelTextField!
    
    @IBOutlet weak var resend_otpBtn: UIButton!
    
    var dictData : [String: String]?
    var otpGet = 0
    var otpGetForgot = false
    
    var timer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtotpOne.addTarget(self, action: #selector (textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtOptTwo.addTarget(self, action:  #selector (textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtOtpthree.addTarget(self, action:  #selector (textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtOtpFour.addTarget(self, action:  #selector (textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtotpOne.textAlignment = .center
        txtOptTwo.textAlignment = .center
        txtOtpthree.textAlignment = .center
        txtOtpFour.textAlignment = .center
      //  self.addAlertView(appName, message: "\(otpGet)", buttonTitle: ALERTS.kAlertOK)
        // Do any additional setup after loading the view.
        
        self.resend_otpBtn.isHidden = true
//        timer = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true, block: { (timer) in
//            self.resend_otpBtn.isHidden = false
//        })
        
    }
    
    
    @IBAction func didTapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    @IBAction func didTapVerify(_ sender: Any) {
        if txtotpOne.text?.count == 1 && txtOptTwo.text?.count == 1 && txtOtpthree.text?.count == 1 && txtOtpFour.text?.count == 1 {
            
            let newNum = (txtotpOne.text!) + (txtOptTwo.text!) + (txtOtpthree.text!) + (txtOtpFour.text!)
         //   let enterOTP = txtotpOne.text! + txtOptTwo.text! + txtOtpthree.text! + txtOtpFour.text!
            
            
            if newNum == String(describing:otpGet){
                if otpGetForgot{
                    let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                    newviewController.dictData = dictData
                    self.navigationController?.pushViewController(newviewController, animated: true)
//                    let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KForgot)")!
//                    MBProgressHUD.showAdded(to: self.view, animated: true)
//                    self.CallAPIForgot(APIurl: url, dictData: dictData)
                }else{
//                    let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KSIGNUP)")!
//                    MBProgressHUD.showAdded(to: self.view, animated: true)
//                    self.CallAPI(APIurl: url, dictData: dictData)
                    
                    let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.VERIFY_OTP)")!
                    let email = self.dictData?["email"] ?? "NA"
                    let param : [String:String] = ["email": email, "otp": newNum]
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    self.verifyOtp(APIurl: url, dictData: param)
                }
            }
            else{
                self.addAlertView(appName, message: "Incorrect OTP", buttonTitle: ALERTS.kAlertOK)
            }
        }
    }
    
    func verifyOtp (APIurl: URL,dictData : Parameters){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                if responseObject.result.error == nil{
                    if responseObject.response?.statusCode == 200{
                        if let JSON = responseObject.result.value as? NSDictionary
                        {
                            if let statusData = JSON["status"] as? Int{
                                if statusData == 1{
                                    let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KSIGNUP)")!
                                    self.CallAPI(APIurl: url, dictData: self.dictData)
                                }else{
                                    
                                }
                            }
                        }
                    }
                }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
        textField.placeholder = nil
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldShouldReturn")
    }
    
    @IBAction func didTapResendOTP(_ sender: Any) {
       // "otp":"\((newDict!["otp"] as! NSString) as String)","otp_verify"
       var url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KSIGNUP)")!
        if otpGetForgot{
            dictData!["otp"] = ""
            url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KForgot)")!
        }else{
            dictData!["otp"] = ""
            dictData!["otp_verify"] = "0"
        }
        //
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIResendOTP(APIurl: url, dictData: dictData!)
      //  self.addAlertView(appName, message: "\(otpGet)", buttonTitle: ALERTS.kAlertOK)
    }
    
    func CallAPIResendOTP (APIurl: URL,dictData : Parameters){
        Alamofire.request(APIurl, method: .post, parameters: dictData, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(responseObject)
                if responseObject.result.error == nil{
                    if responseObject.response?.statusCode == 200{
                        if let JSON = responseObject.result.value as? NSDictionary
                        {
                           // let message = JSON["message"] as? String ?? "Something went wrong."
                            if let statusData = JSON["status"] as? Int{
                                if statusData == 1{
                                    let newDict = JSON["data"] as? NSDictionary
                                    if self.otpGetForgot == false{
                                         self.dictData!["otp_verify"] = "1"
                                    }
                                    self.otpGet = Int("\(newDict!["otp"]!)")!
                                    self.dictData!["otp"] =  newDict!["otp"] as? String
                                  //  self.addAlertView(appName, message: message, buttonTitle: ALERTS.kAlertOK)
                                  //  self.resend_otpBtn.isHidden = true
                                    
                                }else{
                                    
                                }}}}
        }
                
        }}
    
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if text?.utf16.count == 1 {
            switch textField{
            case txtotpOne:
                txtOptTwo.becomeFirstResponder()
            case txtOptTwo:
                txtOtpthree.becomeFirstResponder()
            case txtOtpthree:
                txtOtpFour.becomeFirstResponder()
            case txtOtpFour:
                txtOtpFour.resignFirstResponder()
            default:
                break
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 1
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    func CallAPI(APIurl: URL,dictData : [String:String]?){
        // func CallApiwithParameter(APIurl: URL,dictData : Parameters){
        
        print(APIurl)
        print(dictData)
       
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        print(JSON)
                                        QLSharedPreference.sharedInstance.saveUserData(JSON)
                                        QLSharedPreference.sharedInstance.setLoginStatus(true)
                                        QLSharedPreference.sharedInstance.intrestType("0")
                                        
                                        currentUser = User1.init(dictionary:JSON)
                                          let newviewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateProfileTableVC") as! CreateProfileTableVC
                                        self.navigationController?.pushViewController(newviewController, animated: true)
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as? NSString as! String , buttonTitle: ALERTS.kAlertOK)
                                        
                                        print()
                                    }
                                }
                                
                                //QLSharedPreference.sharedInstance.setLoginStatus(true)
                                //QLSharedPreference.sharedInstance.saveUserData(JSON)
                                //QLSharedPreference.sharedInstance.saveUserId(String(describing: result.value(forKey: "id")!))
                                //                                currentUser = User.init(dictionary:JSON)
                                //                                if currentUser.isSuccess!{
                                //                                    self.navigatToHomeScreen()
                                //                                }else{
                                //                                    self.addAlertView(appName, message: currentUser.message!, buttonTitle: ALERTS.kAlertOK)
                                //                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        //  }
    }
    
}

