//
//  HelpSupportVC.swift
//  Burzz
//
//  Created by Rajat on 21/07/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HelpSupportVC: UIViewController {
    var listArray = [String]()
    
     @IBOutlet weak var bannerAdsView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        bannerAdsView.adUnitID = "ca-app-pub-8168533226721388/4622352003"
        bannerAdsView.rootViewController = self
        bannerAdsView.load(GADRequest())
        
        
        listArray = ["Contact us","FAQ","Privacy Policy","Terms & Conditions", "User's Guidelines"];
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
}
extension HelpSupportVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath)
        
        let categoryName : UILabel = cell.viewWithTag(1) as! UILabel
        let mainView : UIView = cell.viewWithTag(2)!
        categoryName.text = listArray[indexPath.row]
        mainView.layer.borderColor = UIColor.init(red: 236.0/255.0, green:  236.0/255.0, blue:  236.0/255.0, alpha: 1.0).cgColor
        mainView.layer.cornerRadius = mainView.frame.size.height/2
        mainView.layer.borderWidth = 1.0
        return cell
    }
}

extension HelpSupportVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0{
////            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchDetailHost") as! SearchDetailHost
////            self.present(nextViewController, animated:true, completion:nil)
//        }
         if indexPath.row == 0{
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ContactusVC") as! ContactusVC
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else{
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesVC
            nextViewController.fromScreen = indexPath.row + 1
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
    }
}
