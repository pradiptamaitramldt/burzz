//
//  SettingsModel.swift
//  Burzz
//
//  Created by Pradipta Maitra on 27/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import Foundation

// MARK: - CreateReferelCodeResponseModel
struct CreateReferelCodeResponseModel: Codable {
    let status: Bool?
    let message, data: String?
    let isIos, timestamp: Int?

    enum CodingKeys: String, CodingKey {
        case status, message, data
        case isIos = "is_ios"
        case timestamp
    }
}
