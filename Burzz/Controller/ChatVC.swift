//
//  ChatVC.swift
//  Burzz
//
//  Created by MAC MINI on 01/08/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
import Quickblox
import Firebase
import SwiftyJSON
import SDWebImage
import SimpleImageViewer
//import MBProgressHUD
import QuickbloxWebRTC
import FittedSheets
import Alamofire

class ChatVC: UIViewController,QBChatDelegate {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnVedioCall: UIButton!
    @IBOutlet weak var btnAudioCalling: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userProfilePic: UIImageView!
    @IBOutlet weak var btnDropDown: UIButton!
    
    var responseMatch : JSON = []
    var walletBalance: Int = 0
    
    // for Cheat Data
    var friend_Name = ""
    var friend_id = 0
    var profile_picture = ""
    var friend_quickblox_id = 0
    var userType = ""  // old or new
    var matchId = ""
    
    var userProType = 0
    
    var arrMessage : [[String: AnyObject]] = []
    //  var arrMessage = [Dictionary<String, Any>]()
    var chatRespone : JSON = []
    var ref: DatabaseReference!
    var player: AVAudioPlayer?
    var alert : UIAlertController?
    var isAudio: Bool = false
    
    
    let opponentIds = [49734756]
    open var opponets: [QBUUser]?
    var QBCurrentUser: QBUUser?
    var users: [String : String]?
    var videoCapture: QBRTCCameraCapture?
    var session: QBRTCSession?
    
    var baseSession :QBRTCBaseSession?
    
    var videoTrack : QBRTCVideoTrack?
    var userID : NSNumber = 0
    
    //  var selectedQuickBloxUserId = 0
    
    //MARK: - Properti
    var userId = ""
    var textDataType = "text"
    var textData = ""
    var imageUploadUrl = ""
    var name = ""
    
    var callType = "call"
    
    var selfUser = QBUUser()
    var secondPartyUser = QBUUser()
    var quickblox_id2 = String()
    var profile_picture2 = String()
    
    
    let optionsDrops = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.optionsDrops
        ]
    }()
    
    var optionArray = ["View Profile", "Block & Report", "Unmatch"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        print(friend_Name)
        print(friend_id)
        print(friend_quickblox_id)
        print(profile_picture)
        
        userId = currentUser.result!.id!
        QBChat.instance.addDelegate(self)
        observeMessage()
        
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.checkProUser)")!
        
        let dict : [String:String] = ["user_id": "\(friend_id)"]
        
        CallAPIProUser(APIurl: url, dictData: dict)
        
        self.userProfilePic.layer.cornerRadius = self.userProfilePic.frame.size.width / 2
        self.userProfilePic.layer.borderWidth = 2
        self.userProfilePic.layer.borderColor = UIColor.darkGray.cgColor
        self.userProfilePic.clipsToBounds = true
        
        let profileImage = profile_picture
        let URimgUrl1 = profileImage.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
        userProfilePic.sd_setImage(with: URL(string: URimgUrl1), placeholderImage: UIImage(named:"looking_for2"))
        
        lblUserName.text = friend_Name
        
        tableView.register(UINib(nibName: "SendMessageCell", bundle: nil), forCellReuseIdentifier: "sendMessage")
        
        tableView.register(UINib(nibName: "SenderImageMessageCell", bundle: nil), forCellReuseIdentifier: "SenderImageMessageCell")
        
        tableView.register(UINib(nibName: "RecievedMessageCell", bundle: nil), forCellReuseIdentifier: "recievedMessage")
        
        tableView.register(UINib(nibName: "ReciverImageMessageCell", bundle: nil), forCellReuseIdentifier: "ReciverImageMessageCell")
        
        tableView.register(UINib(nibName: "CallTableViewCell", bundle: nil), forCellReuseIdentifier: "CallTableViewCell")
        
        
        //        if isAudio {
        //            configureAudio()
        //        } else {
        //            cofigureVideo()
        //        }
        //        if userID != 0 {
        //      //      self.commonRemote(baseSession!, videoTrack: videoTrack!, fromUser: userID)
        //        } else {
        //            // SVProgressHUD.show(withStatus: "Calling...")
        //            //  self.login(userLogin: "shivam90", password: "shivam@123")
        //
        //            let dataUserDefault = UserDefaults.standard
        //            let quickBoxId = dataUserDefault.object(forKey: "quick_blox_id") as! String
        //            QBRequest.user(withID: UInt(quickBoxId)!, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
        //                // Successful response with user
        //                self.login(userLogin: user.login!, password: "qardiyo_user")
        //            }, errorBlock: {(_ response: QBResponse) -> Void in
        //                // Handle error
        //            })
        //        }
        
        loginInQB()
        
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        // self.composeMessage()
    //        observeMessage()
    //
    //    }
    
    
    func setUIElement()
    {
        self.bottomView.layer.borderWidth = 2.0
        self.bottomView.layer.borderColor = UIColor.gray.cgColor
        self.bottomView.layer.cornerRadius = self.bottomView.frame.size.height*0.50
    }
    
    func viewWalletBalance() {
        let walletRepository = WalletRepository()
        walletRepository.getWalletBalance(vc: self) { (response, success, error) in
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                let data = response.data
                self.walletBalance = data?.bal ?? 0
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    
    private func send(withMessageText text: String) {
        let message = QBChatMessage.markable()
        message.text = textView.text
        message.senderID = selfUser.id
        message.dialogID = String(secondPartyUser.id)
        message.deliveredIDs = [(NSNumber(value: selfUser.id))]
        message.readIDs = [(NSNumber(value: selfUser.id))]
        message.dateSent = Date()
        message.customParameters["save_to_history"] = true
        //  sendMessage(message: message)
    }
    
    //    func composeMessage()  {
    //
    //        //let dict : [String : Any] = ["chatt" : msgTextView.text ]
    //
    //       // let date = Date().localizedDescription
    //
    //
    //        let ref = Database.database().reference().child("message").child("chat123")
    //
    //
    //        let childRef = ref.childByAutoId()
    //        //let values = ["msg" : self.msgTextView.text] as [String : Any]
    //
    //       // childRef.updateChildValues(values)
    //
    //    }
    
    
    // Button Actions
    @IBAction func click_vedio_Calling(_ sender : Any)
    {
        
        // update code after live means uncomment validation
        
        /*     if userProType == 0{
         
         let alert = UIAlertController(title: "Sorry!", message: "Can't make video call since \(friend_Name) is not pro user.", preferredStyle: .alert)
         
         alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
         
         self.present(alert, animated: true)
         
         }else{
         
         textDataType = "text"
         textData = "Video Call"
         composeMessage()
         composeMessageSenserReciver()
         
         if userType == "new"{
         let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.updateMatch)")!
         let dict : [String:String] = ["userId": currentUser.result!.id!, "match_id": "\(matchId)"]
         CallAPIMatchUser(APIurl: url, dictData: dict)
         }
         
         navigateToCall(isAudio: false)
         }
         */
        
        // after uncomment above code delete below code
        
        textDataType = "Video Call"
        textData = "Video Call"
        composeMessage()
        composeMessageSenserReciver()
        
        if userType == "new"{
            let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.updateMatch)")!
            let dict : [String:String] = ["userId": currentUser.result!.id!, "match_id": "\(matchId)"]
            CallAPIMatchUser(APIurl: url, dictData: dict)
        }
        
        navigateToCall(isAudio: false)
        
    }
    
    @IBAction func click_to_audio_calling(_ sender : Any)
    {
        
        // update code after live means uncomment validation
        
        /*        if userProType == 0{
         
         let alert = UIAlertController(title: "Sorry!", message: "Can't make audio call since \(friend_Name) is not pro user.", preferredStyle: .alert)
         
         alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
         
         self.present(alert, animated: true)
         
         }else{
         
         textDataType = "text"
         textData = "Audio Call"
         composeMessage()
         composeMessageSenserReciver()
         
         if userType == "new"{
         let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.updateMatch)")!
         let dict : [String:String] = ["userId": currentUser.result!.id!, "match_id": "\(matchId)"]
         CallAPIMatchUser(APIurl: url, dictData: dict)
         }
         navigateToCall(isAudio: true)
         }
         */
        
        // after uncomment above code delete below code
        
        textDataType = "Audio Call"
        textData = "Audio Call"
        composeMessage()
        composeMessageSenserReciver()
        
        if userType == "new"{
            let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.updateMatch)")!
            let dict : [String:String] = ["userId": currentUser.result!.id!, "match_id": "\(matchId)"]
            CallAPIMatchUser(APIurl: url, dictData: dict)
        }
        navigateToCall(isAudio: true)
        
        
        
    }
    
    @IBAction func btnDropDownTap(_ sender: Any) {
        
        // btnDropDown.placeholder = "Wifi List"
        btnDropDown.addTarget(self, action: #selector(DropDown1(sender: )), for: .touchUpInside)
        
    }
    
    
    @objc func DropDown1 (sender : UIButton){
        self.view.endEditing(true)
        self.optionsDrops.anchorView = sender
        self.optionsDrops.bottomOffset = CGPoint(x: -100, y: sender.bounds.height + 8)
        self.optionsDrops.dataSource.removeAll()
        self.optionsDrops.direction = .any
        self.optionsDrops.dataSource.append(contentsOf: self.optionArray)
        
        // Action triggered on selection
        self.optionsDrops.selectionAction = { [unowned self] (index, item) in
            print("\(index)",item)
            
            print(index)
            print(item)
            
            if index == 0{
                
                let VC1 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                VC1.friend_id = self.friend_id
                self.present(VC1, animated:true, completion: nil)
                
            }else if index == 1{
                
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "CustomPopUpVC") as! CustomPopUpVC
                //  controller.blockId = serverArray[currentIndexDisplay].user_id!
                let sheetController = SheetViewController(controller: controller, sizes: [.fixed(440), .fixed(440), .halfScreen, .halfScreen])
                
                // Adjust how the bottom safe area is handled on iPhone X screens
                sheetController.blurBottomSafeArea = false
                sheetController.adjustForBottomSafeArea = true
                
                // Turn off rounded corners
                sheetController.topCornersRadius = 0
                
                // Make corners more round
                sheetController.topCornersRadius = 15
                
                // Disable the dismiss on background tap functionality
                sheetController.dismissOnBackgroundTap = false
                
                // Extend the background behind the pull bar instead of having it transparent
                sheetController.extendBackgroundBehindHandle = false
                
                // It is important to set animated to false or it behaves weird currently
                self.present(sheetController, animated: false, completion: nil)
                
            }else if index == 2{
                
                let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.unMatch)")!
                let dict : [String:String] = ["user_id": currentUser.result!.id!, "unfollow_user_id" : "\(self.friend_id)"]
                self.CallAPIProfile2(APIurl: url, dictData: dict)
            }
            // self.txtDropDown1.text = item
        }
        self.optionsDrops.show()
    }
    
    
    func CallAPIProfile2(APIurl: URL,dictData : [String:String]?){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    //MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            let swiftyJsonVar = JSON(response.result.value!)
                            print(swiftyJsonVar)
                            
                            let status = swiftyJsonVar["status"].boolValue
                            
                            if status{
                                
                                print(swiftyJsonVar)
                                MBProgressHUD.hide(for: self.view, animated: true)
                                
                                let alert = UIAlertController(title: swiftyJsonVar["message"].stringValue, message: "", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                
                                self.present(alert, animated: true) {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
                                        guard self?.presentedViewController == alert else { return }
                                        self?.dismiss(animated: true, completion: nil)
                                    }
                                }
                                
                            }else{
                                self.addAlertView(appName, message: swiftyJsonVar["message"].stringValue , buttonTitle: ALERTS.kAlertOK)
                            }
                        }
                    }
                    
                })
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
                print("1")
            }
        })
    }
    
    
    func CallAPIProUser(APIurl: URL,dictData : [String:String]?){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    //MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            let swiftyJsonVar = JSON(response.result.value!)
                            print(swiftyJsonVar)
                            
                            let status = swiftyJsonVar["status"].boolValue
                            
                            if status{
                                
                                print(swiftyJsonVar)
                                let userType = swiftyJsonVar["data"]["pro_user"].intValue
                                
                                if userType == 1{
                                    self.userProType = 1
                                }
                                else{
                                    self.userProType = 0
                                }
                                MBProgressHUD.hide(for: self.view, animated: true)
                                
                            }else{
                                
                                self.addAlertView(appName, message: swiftyJsonVar["message"].stringValue , buttonTitle: ALERTS.kAlertOK)
                            }
                        }
                    }
                    
                })
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
                print("1")
            }
        })
    }
    
    func CallAPIPayment(APIurl: URL,dictData : [String:String]?){
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        
                                        self.viewWalletBalance()
                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
    }
    
    
    @IBAction func click_to_send_message(_ sender : Any)
    {
        textDataType = "text"
        if(textView.text == "") || (textView.text == "Type a message")
        {
            //self.alert(message: "enter Some text")
        }
        else
        {
            if self.walletBalance < 10 {
                let alert = UIAlertController(title: "Burzz", message: "You have insufficient balance to send message! Please Recharge your wallet with coins.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }else{
                let alert = UIAlertController(title: "Burzz", message: "Text message will cost 10 coins.\nWould you like to continue?", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    let url = URL(string:  "http://3.215.188.72/index.php/data_model/plans/Plans/savetransactions")//"https://demos.mydevfactory.com/debarati/burzz/index.php/data_model/plans/Plans/savetransactions")
                    
                    let dicURL : [String : String] = ["user_id": currentUser.result!.id!, "transactions_type":"debit","transaction_id":"","transaction_from":"internal", "cost": "0", "coin": "10"]
                    
                    self.CallAPIPayment(APIurl: url!, dictData: dicURL)
                    self.composeMessage()
                    self.composeMessageSenserReciver()
                    
                    if self.userType == "new"{
                        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.updateMatch)")!
                        let dict : [String:String] = ["user_id": currentUser.result!.id!, "match_id": "\(self.matchId)"]
                        self.CallAPIMatchUser(APIurl: url, dictData: dict)
                    }
                    
                    self.textView.text = ""
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                
                self.present(alert, animated: true)
            }
        }
    }
    
    @IBAction func btn_click_to_send_Images(_ sender: Any) {
        self.ptototickerContollerCall()
        self.textDataType = "image"
        
    }
    
    func ptototickerContollerCall() {
        
        let actionSheet = UIAlertController(title: "Choose Media", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func composeMessage()  {
        
        let ref = Database.database().reference().child("chat_module").child("social_private").child(userId).child("\(friend_id)")
        let childRef = ref.childByAutoId()
        let timeStamp = currentTimeInMilliSeconds()
        
        if textDataType == "text"{
            textData = textView.text!
        }else{
            textData = imageUploadUrl
        }
        
        let values = [ "from" : "\(userId)",
            "message" : textData,
            "seen" : false,
            "status" : "1",
            "time" : timeStamp,
            "type" : textDataType] as [String : Any]
        
        let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.chatNotification)")!
        
        let dict : [String:String] = ["sender_id": currentUser.result!.id!, "recipient_id": "\(friend_id)", "msg" : textData, "type" : textDataType]
        
        CallAPIMatchUser(APIurl: url, dictData: dict)
        
        childRef.updateChildValues(values)
        
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    func composeMessageSenserReciver()  {
        
        let ref = Database.database().reference().child("chat_module").child("social_private").child("\(friend_id)").child(userId)
        
        let childRef = ref.childByAutoId()
        let timeStamp = currentTimeInMilliSeconds()
        
        if textDataType == "text"{
            textData = textView.text!
        }else{
            textData = imageUploadUrl
        }
        
        let values = [ "from" : "\(userId)",
            "message" : textData,
            "seen" : false,
            "status" : "1",
            "time" : timeStamp,
            "type" : textDataType] as [String : Any]
        
        childRef.updateChildValues(values)
        
        MBProgressHUD.hide(for: self.view, animated: false)
    }
    
    
    func setSeenMessage(dict:Dictionary<String,Any>)  {
        print(dict)
        Database.database().reference().child("chat_module").child("social_private").child("\(friend_id)").child(userId).setValue(dict, withCompletionBlock: { (error, ref) in
            print(ref)
            print(error as Any)
        })
    }
    
    
    func updateAnyFirebaseVale(_ param :  String){
        
        
        
        //            let val = Database.database().reference().child("chat_module").child("social_private").child("\(friend_id)").child(userId)
        //            val.updateChildValues(["seen" : true])
        
        
    }
    
    
    
    
    
    func observeMessage()
    {
        
        Database.database().reference().child("chat_module").child("social_private").child("\(friend_id)").child(userId).observe(.childAdded, with: { (snapshot) in
            
            let dic = snapshot.value as? [String: AnyObject]
            
            if dic == nil{
                return
            }
            
            var dict = dic!
            dict["seen"] = true as AnyObject
            let id = snapshot.key
            
            let FrindId = Int(dic!["from"] as? String ?? self.userId)!
            // if self.friend_id == FrindId{
            let seen = dic!["seen"] as? Int ?? 0
            if seen == 0{
                Database.database().reference().child("chat_module").child("social_private").child(self.userId).child("\(self.friend_id)").child("\(id)").setValue(dict, withCompletionBlock: { (error, ref) in
                    print(ref)
                    print(error as Any)
                })
            }
            // }
            
            self.arrMessage.append(dic!)
            
            let type = dic!["type"] as? String
            
            if type == "Video Call"{
                self.callType = "Video call"
            }else if type == "Audio Call"{
                self.callType = "Audio call"
            }
            
            print(dic!)
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath.init(row: self.arrMessage.count - 1, section: 0), at: UITableView.ScrollPosition.bottom , animated: false)
            
        })
        
    }
    
    func CallAPINotification(APIurl: URL,dictData : [String:String]?){
        //    MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    //MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            let swiftyJsonVar = JSON(response.result.value!)
                            print(swiftyJsonVar)
                            let status = swiftyJsonVar["status"].boolValue
                            
                            if status{
                                
                                
                                
                            }else{
                                
                                self.addAlertView(appName, message: swiftyJsonVar["message"].stringValue , buttonTitle: ALERTS.kAlertOK)
                            }
                        }
                    }
                    
                })
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
                print("1")
            }
        })
    }
    
    
    func CallAPIMatchUser(APIurl: URL,dictData : [String:String]?){
        //    MBProgressHUD.showAdded(to: self.view, animated: true)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    //MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            let swiftyJsonVar = JSON(response.result.value!)
                            print(swiftyJsonVar)
                            let status = swiftyJsonVar["status"].boolValue
                            
                            if status{
                                
                                print(swiftyJsonVar)
                                MBProgressHUD.hide(for: self.view, animated: true)
                                
                            }else{
                                
                                self.addAlertView(appName, message: swiftyJsonVar["message"].stringValue , buttonTitle: ALERTS.kAlertOK)
                            }
                        }
                    }
                    
                })
            case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
                print("1")
            }
        })
    }
    
    
    
    func currentTimeInMilliSeconds()-> Int
    {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    
    
    @IBAction func click_to_back(_ sender : Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
}


extension ChatVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(arrMessage)
        
        let userIDDict = arrMessage[indexPath.row]
        print(userIDDict)
        let userIDSender = "\(userIDDict["from"]!)"
        //****************** self side ********************
        if userId == String(userIDSender){
            
            let textType = userIDDict["type"] as! String
            
            if textType == "text"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "sendMessage") as! SendMessageCell
                cell.messageContent?.text = userIDDict["message"] as? String
                
                
                cell.messageContent?.layer.masksToBounds = true
                cell.messageContent?.layer.cornerRadius = 5
                
                return cell
                
            }
            else if textType == "Audio Call"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CallTableViewCell") as! CallTableViewCell
                
                
                let timeStamp = userIDDict["time"] as? Int
                
                if timeStamp == nil{
                    
                    
                    cell.lblCallDetail.text  = "Outgoing audio call"
                }
                else{
                    
                    let date = Date(timeIntervalSince1970: Double(timeStamp!/1000))
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                    dateFormatter.timeZone = .current
                    let localDate : String = dateFormatter.string(from: date)
                    let datenow = dateFormatter.date(from: localDate)
                    let timeValue = (datenow)!.getElapsedInterval()
                    cell.lblCallDetail.text = "Outgoing audio call, \(timeValue)"
                }
                
                cell.callView.layer.cornerRadius = 10
                
                return cell
                
            }
            else if textType == "Video Call"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CallTableViewCell") as! CallTableViewCell
                
                
                let timeStamp = userIDDict["time"] as? Int
                
                if timeStamp == nil{
                    
                    
                    cell.lblCallDetail.text  = "Outgoing video call"
                }
                else{
                    
                    let date = Date(timeIntervalSince1970: Double(timeStamp!/1000))
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                    dateFormatter.timeZone = .current
                    let localDate : String = dateFormatter.string(from: date)
                    let datenow = dateFormatter.date(from: localDate)
                    let timeValue = (datenow)!.getElapsedInterval()
                    cell.lblCallDetail.text = "Outgoing video call, \(timeValue)"
                }
                
                cell.callView.layer.cornerRadius = 10
                
                return cell
                
            }
            else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderImageMessageCell") as! SenderImageMessageCell
                
                cell.bgView?.layer.masksToBounds = true
                cell.bgView?.layer.cornerRadius = 10
                
                cell.imageSender?.layer.masksToBounds = true
                cell.imageSender?.layer.cornerRadius = 5
                
                cell.imageSender.isUserInteractionEnabled = true
                
                let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
                cell.imageSender.addGestureRecognizer(tapGesture)
                
                let profileImage = userIDDict["message"] as! String
                let yourString = profileImage
                let URimgUrl1 = yourString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
                cell.imageSender.sd_setImage(with: URL(string: URimgUrl1))
                
                return cell
                
            }
            
        }
            
            //******************************** Opponent **********************
            //Audio Call
        else{
            
            print(userIDDict)
            
            
            
            
            if userIDDict["seen"] as! Int == 0{
                
                
                
            }
            
            
            
            
            let textType = userIDDict["type"] as! String
            if textType == "text"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "recievedMessage") as! RecievedMessageCell
                cell.messageContent?.layer.masksToBounds = true
                cell.messageContent?.layer.cornerRadius = 5
                cell.messageContent.textColor = UIColor.white
                cell.messageContent?.text = userIDDict["message"] as? String
                
                
                return cell
                
            }
            else if textType == "Video Call"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CallTableViewCell") as! CallTableViewCell
                
                cell.callView.layer.cornerRadius = 10
                
                let timeStamp = userIDDict["time"] as? Int
                
                if timeStamp == nil{
                    
                    cell.lblCallDetail.text = ""
                    cell.lblCallDetail.text  = "Incoming video call"
                }else{
                    
                    let date = Date(timeIntervalSince1970: Double(timeStamp!/1000))
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                    dateFormatter.timeZone = .current
                    let localDate : String = dateFormatter.string(from: date)
                    let datenow = dateFormatter.date(from: localDate)
                    let timeValue = (datenow)!.getElapsedInterval()
                    cell.lblCallDetail.text = "Incoming video call, \(timeValue)"
                }
                
                return cell
            }
            else if textType == "Audio Call"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CallTableViewCell") as! CallTableViewCell
                
                cell.callView.layer.cornerRadius = 10
                
                let timeStamp = userIDDict["time"] as? Int
                
                if timeStamp == nil{
                    
                    cell.lblCallDetail.text = ""
                    cell.lblCallDetail.text  = "Incoming audio call"
                }else{
                    
                    let date = Date(timeIntervalSince1970: Double(timeStamp!/1000))
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                    dateFormatter.timeZone = .current
                    let localDate : String = dateFormatter.string(from: date)
                    let datenow = dateFormatter.date(from: localDate)
                    let timeValue = (datenow)!.getElapsedInterval()
                    cell.lblCallDetail.text = "Incoming audio call, \(timeValue)"
                }
                
                return cell
                
            }
            else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReciverImageMessageCell") as! ReciverImageMessageCell
                
                cell.bgView?.layer.masksToBounds = true
                cell.bgView?.layer.cornerRadius = 10
                
                cell.imageReciver?.layer.masksToBounds = true
                cell.imageReciver?.layer.cornerRadius = 5
                
                cell.imageReciver.isUserInteractionEnabled = true
                let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
                cell.imageReciver.addGestureRecognizer(tapGesture)
                
                let profileImage = userIDDict["message"] as! String
                let yourString = profileImage
                let URimgUrl1 = yourString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
                cell.imageReciver.sd_setImage(with: URL(string: URimgUrl1))
                
                return cell
                
            }
        }
    }
    
    
    @objc func imgTap(tapGesture: UITapGestureRecognizer) {
        let imgView = tapGesture.view as! UIImageView
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imgView
        }
        let imageViewerController = ImageViewerController(configuration: configuration)
        present(imageViewerController, animated: true)
    }
    
}

extension ChatVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "Camera is not opening", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    
    func photoLibrary()
    {
        let myPicController = UIImagePickerController()
        myPicController.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        myPicController.allowsEditing = true
        myPicController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPicController, animated: true, completion: nil)
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        let dic : [String : Any] = ["name" : image,"type":"image","identifier" :"image","path" : APIManager.sharedInstance.S3BucketNameProfileImage]
        if self.walletBalance < 50 {
            let alert = UIAlertController(title: "Burzz", message: "You have insufficient balance to send image! Please Recharge your wallet with coins.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }else{
            let alert = UIAlertController(title: "Burzz", message: "Sending image will cost 50 coins.\nWould you like to continue?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                let url = URL(string:  "http://3.215.188.72/index.php/data_model/plans/Plans/savetransactions")//"https://demos.mydevfactory.com/debarati/burzz/index.php/data_model/plans/Plans/savetransactions")
                
                let dicURL : [String : String] = ["user_id": currentUser.result!.id!, "transactions_type":"debit","transaction_id":"","transaction_from":"internal", "cost": "0", "coin": "50"]
                
                self.CallAPIPayment(APIurl: url!, dictData: dicURL)
                self.hitUploadImageToSSS(dataDict: dic)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
        
        
        //        arrImg[currentInd] = image
        //       let ImageDate : [String:Any] = ["image": arrImg,"index": currentInd ]
        //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataDownloadCompleted"), object: ImageDate, userInfo: nil)
        //        UserDefaults.standard.set(0, forKey: "rotetedBy")
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    //
    //    @objc func uploadImage(sender: Notification){
    //
    //        let arr = sender.object as! [String: Any]
    //        let arrimage = arr["image"] as? NSArray
    //        let index = arr["index"] as? Int
    //
    //    }
    
    
    func hitUploadImageToSSS(dataDict:  [String : Any]) {
        
        //   NotificationCenter.default.post(name: Notification.Name("ShowLoader"), object: 1)
        NotificationCenter.default.removeObserver(self, name: .dataDownloadCompleted, object: nil)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        AmazonSSS.uploadDocumentToS3Server(dataDict: dataDict as NSDictionary) { (result, response, error, errorMessage) in
            //      MBProgressHUD.hide(for: self.view, animated: true)
            //self.activityIndicatorView.stopAnimating()
            if( error==nil && result == true){
                
                print(response!)
                var imageUrl = String(describing: response!)
                imageUrl = imageUrl.replacingOccurrences(of: "burzz/", with: "burzz//")
                self.imageUploadUrl = imageUrl
                
                self.composeMessage()
                self.composeMessageSenserReciver()
                
                if self.userType == "new"{
                    let url = URL(string: "\(APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.updateMatch)")!
                    let dict : [String:String] = ["match_id": "\(self.matchId)"]
                    self.CallAPIMatchUser(APIurl: url, dictData: dict)
                }
            }
            else{
                
                
            }
            
        }
    }
    
    
    func loginInQB() {
        
        let dataUserDefault = UserDefaults.standard
        //        let senderInfo = dataUserDefault.object(forKey: "quick_blox_id") as! Dictionary<String,Any>
        //       let quickBoxId = senderInfo["quick_blox_id"] as! String!
        //  let quickBoxId = dataUserDefault.object(forKey: "quick_blox_id") as! String
        
        let quickBloxId = currentUser.result!.quickblox_id
        
        QBRequest.user(withID: UInt(quickBloxId)!, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
            
            // Successful response with user
            self.login(userLogin: user.login!, password: "12345678")
        }, errorBlock: {(_ response: QBResponse) -> Void in
            // Handle error
        })
    }
    func disconnect(){
        
        if QBChat.instance.isConnected {
            QBChat.instance.disconnect { (error) in
                
            }
        }
        
    }
    
    
    
    func login(userLogin: String, password: String) {
        QBRequest.logIn(withUserLogin: userLogin, password: password, successBlock:{ r, user in
            // self.currentUser = user
            QBChat.instance.connect(with: user) { err in
                //let logins = self.users?.keys.filter {$0 != user.login}
                QBRequest.user(withLogin: user.login!, successBlock: { (r, u) in
                    QBRTCClient.initializeRTC()
                    QBRTCClient.instance().add(self)
                    //self.configureBarButtons()
                }, errorBlock: { (rese) in
                })
            }
        })
    }
    
    
    
}


extension ChatVC:  QBRTCClientDelegate{
    
    
    //MARK: WebRTC configuration
    func cofigureVideo() {
        QBRTCConfig.mediaStreamConfiguration().videoCodec = .H264
        let videoFormat = QBRTCVideoFormat.init()
        videoFormat.frameRate = 21
        videoFormat.pixelFormat = .format420f
        videoFormat.width = 640
        videoFormat.height = 480
        
        self.videoCapture = QBRTCCameraCapture.init(videoFormat: videoFormat, position: .front)
        self.videoCapture?.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        
        self.videoCapture?.startSession {
            let localView = LocalVideoView.init(withPreviewLayer:(self.videoCapture?.previewLayer)!)
            // self.stackView.addArrangedSubview(localView)
            
            QBRTCAudioSession.instance().currentAudioDevice = .speaker
            
            //QBRTCSoundRouter.instance.currentSoundRoute = QBRTCSoundRouteSpeaker
        }
    }
    
    
    func configureAudio() {
        //        self.stackView.isHidden = false
        QBRTCConfig.mediaStreamConfiguration().audioCodec = .codecOpus
        //Save current audio configuration before start call or accept call
        QBRTCAudioSession.instance().initialize()
        QBRTCAudioSession.instance().currentAudioDevice = .speaker
        //OR you can initialize audio session with a specific configuration
        QBRTCAudioSession.instance().initialize { (configuration: QBRTCAudioSessionConfiguration) -> () in
            
            var options = configuration.categoryOptions
            if #available(iOS 10.0, *) {
                options = options.union(AVAudioSession.CategoryOptions.allowBluetoothA2DP)
                options = options.union(AVAudioSession.CategoryOptions.allowAirPlay)
            } else {
                options = options.union(AVAudioSession.CategoryOptions.allowBluetooth)
            }
            
            configuration.categoryOptions = options
            configuration.mode = AVAudioSession.Mode.videoChat.rawValue
        }
        
    }
    
    
    public func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        
        if self.session == nil {
            self.session = session
            handleIncomingCall()
        }
        
    }
    
    public func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        
        if (session as! QBRTCSession).id == self.session?.id {
            if session.conferenceType == QBRTCConferenceType.video {
                //                self.screenShareBtn.isHidden = false
            } else{
                navigateToCall(isAudio: true)
            }
        }
    }
    
    public func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        
        if session.id == self.session?.id {
            
            self.removeRemoteView(with: userID.uintValue)
            if userID == session.initiatorID {
                self.session?.hangUp(nil)
                // videoTrack = nil
                
            }
        }
    }
    
    
    public func session(_ session: QBRTCBaseSession, receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack, fromUser userID: NSNumber) {
        if (session as! QBRTCSession).id == self.session?.id {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CallVC") as! CallViewController
            nextViewController.friend_QuickBlock_ID = friend_quickblox_id
            nextViewController.currentUserQB =  self.QBCurrentUser
            nextViewController.videoTrack = videoTrack
            nextViewController.userID = userID
            nextViewController.baseSession = session
            nextViewController.session = self.session
            // self.navigationController?.pushViewController(nextViewController, animated: true)
            
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                
                topController.present(nextViewController, animated: true, completion: nil)
                // topController.navigationController?.pushViewController(nextViewController, animated: true)
                
            }
        }
    }
    
    func logOut() {
        QBChat.instance.disconnect { (err) in
            QBRequest .logOut(successBlock: { (r) in
            })
        }
    }
    
    public func sessionDidClose(_ session: QBRTCSession) {
        let dataUserDefault = UserDefaults.standard
        if session.id == self.session?.id {
            // self.callBtn.isHidden = false
            //self.logoutBtn.isEnabled = true
            //            self.screenShareBtn.isHidden = true
            //            let ids = self.opponets?.map({$0.id})
            //
            //             for userID in opponentIds {
            //            let senderInfo = dataUserDefault.object(forKey: "quick_blox_ids") as! Dictionary<String,Any>
            //            let quickBoxId = senderInfo["quick_blox_id"] as? String
            //            self.removeRemoteView(with: UInt(quickBoxId!)!)
            
            self.session = nil
            self.player?.stop()
            alert?.dismiss(animated: true, completion: nil)
            alert = nil
            
            //  }
        }
    }
    
    //MARK: Helpers
    func resumeVideoCapture() {
        // ideally you should always stop capture session
        // when you are leaving controller in any way
        // here we should get its running state back
        //        if self.videoCapture != nil && !(self.videoCapture?.hasStarted)! {
        //            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
        //            self.videoCapture?.startSession(nil)
        //        }
    }
    
    
    func removeRemoteView(with userID: UInt) {
        //        self.stackView.isHidden = false
        //    for view in self.stackView.arrangedSubviews {
        //        if view.tag == userID {
        //            self.stackView.removeArrangedSubview(view)
        //        }
        //    }
    }
    
    
    func handleIncomingCall() {
        
        alert = UIAlertController.init(title: "Incoming \(callType)", message: "Accept ?", preferredStyle: .actionSheet)
        let accept = UIAlertAction.init(title: "Accept", style: .default) { action in
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.session?.acceptCall(nil)
            self.player?.stop()
            
        }
        
        let reject = UIAlertAction.init(title: "Reject", style: .default) { action in
            self.disconnect()
            self.session?.rejectCall(nil)
            self.player?.stop()
            
        }
        
        alert?.addAction(accept)
        alert?.addAction(reject)
        // self.present(alert, animated: true)
        // Utility.showAlertMessage(vc: self, titleStr:"Incomming call", messageStr:"Video call")
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert!, animated: true, completion: nil)
        playSound()
    }
    
    
    func navigateToCall(isAudio: Bool) {
        
        if isAudio == true && self.walletBalance < 50 {
            let alert = UIAlertController(title: "Burzz", message: "You have insufficient balance to make audio call! Please Recharge your wallet with coins.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }else if isAudio == true && self.walletBalance >= 50 {
            
            let alert = UIAlertController(title: "Burzz", message: "Making audio call will cost 50 coins per minute.\nWould you like to continue?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                let url = URL(string:  "http://3.215.188.72/index.php/data_model/plans/Plans/savetransactions")//"https://demos.mydevfactory.com/debarati/burzz/index.php/data_model/plans/Plans/savetransactions")
                
                let dicURL : [String : String] = ["user_id": currentUser.result!.id!, "transactions_type":"debit","transaction_id":"","transaction_from":"internal", "cost": "0", "coin": "50"]
                
                self.CallAPIPayment(APIurl: url!, dictData: dicURL)
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CallVC") as! CallViewController
                
                nextViewController.friend_QuickBlock_ID = self.friend_quickblox_id
                nextViewController.currentUserQB =  self.QBCurrentUser
                nextViewController.isAudio = isAudio
                //  self.navigationController?.pushViewController(nextViewController, animated: true)
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    
                    topController.present(nextViewController, animated: true, completion: nil)
                }
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }else if isAudio == false && self.walletBalance < 100 {
            let alert = UIAlertController(title: "Burzz", message: "You have insufficient balance to make video call! Please Recharge your wallet with coins.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }else if isAudio == false && self.walletBalance >= 100 {
            
            let alert = UIAlertController(title: "Burzz", message: "Making video call will cost 100 coins per minute.\nWould you like to continue?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                let url = URL(string:  "http://3.215.188.72/index.php/data_model/plans/Plans/savetransactions")//"https://demos.mydevfactory.com/debarati/burzz/index.php/data_model/plans/Plans/savetransactions")
                
                let dicURL : [String : String] = ["user_id": currentUser.result!.id!, "transactions_type":"debit","transaction_id":"","transaction_from":"internal", "cost": "0", "coin": "100"]
                
                self.CallAPIPayment(APIurl: url!, dictData: dicURL)
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CallVC") as! CallViewController
                
                nextViewController.friend_QuickBlock_ID = self.friend_quickblox_id
                nextViewController.currentUserQB =  self.QBCurrentUser
                nextViewController.isAudio = isAudio
                //  self.navigationController?.pushViewController(nextViewController, animated: true)
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    
                    topController.present(nextViewController, animated: true, completion: nil)
                }
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "ringtone", withExtension: "wav") else { return }
        do {
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint:AVMediaType.audio.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            player.numberOfLoops = 3
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
}


