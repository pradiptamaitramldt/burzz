//
//  StaticPagesVC.swift
//  Burzz
//
//  Created by mac on 24/07/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit
//import MBProgressHUD
import Alamofire
import WebKit
class StaticPagesVC: UIViewController,WKNavigationDelegate {
    var fromScreen = Int()
    @IBOutlet weak var WebKitView: WKWebView!
    @IBOutlet weak var headerLAbel: UILabel!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        WebKitView.scrollView.bounces = false
       
     
        if fromScreen == 2{

            headerLAbel.text = "FAQ"
            self.WebKitView.load(NSURLRequest(url: NSURL(string: APIManager.sharedInstance.FAQURL)! as URL) as URLRequest)
            
            WebKitView.navigationDelegate = self
         
           
           
        }
        else if fromScreen == 3{
            headerLAbel.text = "Privacy Policy"
            APiCalling(getId: "2")
            
        }
        else if fromScreen == 4{
            headerLAbel.text = "Terms & Conditions"
            APiCalling(getId: "1")
        }
        else
        {
            headerLAbel.text = "User's Guidelines"
            APiCalling(getId: "3")
        }
            
        
        // Do any additional setup after loading the view.
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
         MBProgressHUD.showAdded(to: self.view, animated: true)
     }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func APiCalling(getId : String)
    {
        let url = URL(string:  APIManager.sharedInstance.KBASEURL + APIManager.sharedInstance.KTCPP)
        
        
        let dicURL : [String : String] = ["id":getId ]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.CallAPIStaticPage(APIurl: url!, dictData: dicURL )
        
    }
    
    func CallAPIStaticPage(APIurl: URL,dictData : [String:String]?){
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dictData! {
                print(key , value)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
            }
        }, usingThreshold: UInt64(), to: APIurl, method: .post , headers: nil, encodingCompletion: { (encodingResult) in
            debugPrint(encodingResult)
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON(completionHandler: {(response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.error == nil{
                        if response.response?.statusCode == 200{
                            if let JSON = response.result.value as? NSDictionary
                            {
                                if let statusData = JSON["status"] as? Int{
                                    if statusData == 1{
                                        let data = JSON["data"] as? Array<Any>
                                        let dict = data![0] as! NSDictionary
                                        print(JSON)
                                         self.WebKitView.loadHTMLString(String(describing: dict["description"]!), baseURL: nil)

                                    }else{
                                        self.addAlertView(appName, message: JSON["message"] as! String , buttonTitle: ALERTS.kAlertOK)
                                    }
                                }
                            }
                            
                        }
                    }
                })
            case .failure(_):
                print("1")
            }
        })
        
    }
}

