//
//  WalletRepository.swift
//  Burzz
//
//  Created by Pradipta Maitra on 11/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit

class WalletRepository: NSObject {
    
    var utility = Utility()
    
    func getWalletBalance(vc: UIViewController, completion: @escaping (ViewWalletBalance, Bool, NSError?) -> Void) {
        let request = WalletRequest()
        request.getWalletBalance(vc: vc, hud: false, codableType: APIResponseParentModel.self) { (response, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(ViewWalletBalance.self, from: response! as! Data)
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
