//
//  CoinPackagesRequest.swift
//  Burzz
//
//  Created by Pradipta Maitra on 06/08/20.
//  Copyright © 2020 appsquad. All rights reserved.
//

import UIKit
import Alamofire

class CoinPackagesRequest: NSObject {
    
    let load = Loader()
    
    func getCoinPackages<T : APIResponseParentModel>(vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let postDataUrl = "http://3.215.188.72/index.php/data_model/plans/Plans/getallcoins"//"https://demos.mydevfactory.com/debarati/burzz/index.php/data_model/plans/Plans/getallcoins"
            
            var request = URLRequest(url: URL(string: postDataUrl)!)
            
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            Alamofire.request(postDataUrl, method: .get, parameters: nil, encoding: URLEncoding.default).responseData { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        print(value)
                        if hud {
                            self.load.hide(delegate: vc)
                        }
                        completion(response.result.value, true)
                    }
                case .failure(let error):
                    print(error)
                    if hud {
                        self.load.hide(delegate: vc)
                    }
                }
            }
        }
    }
}
