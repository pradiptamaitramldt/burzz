//
//  APIHelper.swift
//  ParkLinQ
//
//  Created by Shailendra Kumar on 23/01/18.
//  Copyright © 2018 appideasinc. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class APIHelper: NSObject {
    
    static let sharedInstance = APIHelper()
    
    func requestGETURL(_ strURL: String, success:@escaping (JSON, [String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                let resp = responseObject.result.value as! [String: AnyObject]
                success(resJson, resp)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    
    
    
    func requestPOSTWithouAUTHURL(_ strURL : String, params : [String : String]?, headers : [String : String]?, success:@escaping (JSON, [String: AnyObject]) -> Void, failure:@escaping (Error) -> Void){
        
       

        Alamofire.request(strURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON
            {
                (responseObject) -> Void in
                
                print(responseObject)
                
                if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    let resp = responseObject.result.value as! [String: AnyObject]
                    success(resJson, resp)
                }
                if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
        }
    }
    
    func requestPOSTWithHeader(_ strURL : String, params : [String : String]?, headers : [String : String]?, success:@escaping (JSON, [String: AnyObject]) -> Void, failure:@escaping (Error) -> Void){
        
        
        Alamofire.request(strURL, method: .post, parameters: params, headers: headers).responseJSON
            {
                (responseObject) -> Void in
                
                print(responseObject)
                
                if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    let resp = responseObject.result.value as! [String: AnyObject]
                    success(resJson, resp)
                }
                if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
        }
    }
    
    func requestPOSTWithHeaderTypeAny(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON, [String: AnyObject]) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON
            {
                (responseObject) -> Void in
                
                print(responseObject)
                
                if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    let resp = responseObject.result.value as! [String: AnyObject]
                    success(resJson, resp)
                }
                if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
        }
    }
    
    func requestGetWithouAUTHURL(_ strURL : String, params : [String : String]?, headers : [String : String]?, success:@escaping (JSON, [String: AnyObject]) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .get, parameters: params, headers: nil).responseJSON
            {
                (responseObject) -> Void in
                
                print(responseObject)
                
                if responseObject.result.isSuccess {
                    let resJson = JSON(responseObject.result.value!)
                    let resp = responseObject.result.value as! [String: AnyObject]
                    success(resJson, resp)
                }
                if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
        }
    }
    
    
    func multipartRequest(_ strUrl: String, param: [String: String], headers: [String: String]?, image: UIImage?, success: @escaping (JSON,[String: AnyObject]) -> Void, failure: @escaping (Error) -> Void)
    {
        Alamofire.upload(multipartFormData: { (multipartFormData) in
                      for (key, value) in param {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            
            if image != nil
            {
                
                if let data =  image!.pngData()
                {
                    multipartFormData.append(data, withName: "mtAvatar", fileName: "avatar.png", mimeType: "image/png")
                }
            }
 
            
        }, usingThreshold: UInt64.init(), to: strUrl, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error{
                        failure(err)
                        return
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                failure(error)
            }
        }
    }
    
   
    
    
}
