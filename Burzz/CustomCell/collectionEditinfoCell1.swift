//
//  collectionEditinfoCell1.swift
//  Burzz
//
//  Created by Raul Menendez on 03/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class collectionEditinfoCell1: UICollectionViewCell {
    
    @IBOutlet weak var imagView: UIImageView!
    
    @IBOutlet weak var deleteView: UIView!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnImgPic: UIButton!
}
