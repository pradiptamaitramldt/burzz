//
//  SendMessageCell.swift
//  Diabetes
//
//  Created by Charanjeet on 17/05/18.
//  Copyright © 2018 Charanjeet. All rights reserved.
//

import UIKit
import PaddingLabel

class SendMessageCell: UITableViewCell {

    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var messageContent: PaddingLabel!
    @IBOutlet weak var messageStatus: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
   
     override func awakeFromNib() {
        super.awakeFromNib()
        
        
        bgView.layoutIfNeeded()
        
         messageContent.topInset = 10.0
        messageContent.bottomInset = 10.0
        messageContent.leftInset = 12.0
        messageContent.rightInset = 20.0

        
        
     
        
        
       
        
    
    
   
    
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
