//
//  CallTableViewCell.swift
//  Burzz
//
//  Created by Vikasd Gupta on 19/09/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import UIKit

class CallTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var lblCallDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
