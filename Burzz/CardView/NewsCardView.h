//
//  NewsCardView.h
//  InshortsTest
//
//  Created by Vikrant Sharma on 27/12/16.
//  Copyright © 2016 VikrantSharma. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface NewsCardView : UIView
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic) int containerWidth;
@property (nonatomic) int containerHeight;
@property (nonatomic) int containerX;
@property (nonatomic) int containerY;
@property (weak, nonatomic) IBOutlet UIImageView *imgPost;
@property (weak, nonatomic) IBOutlet UILabel *lblDesp;
@property (weak, nonatomic) IBOutlet UILabel *lblTittle;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnAddBookmark;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalFollower;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (weak, nonatomic) IBOutlet UIButton *btnFollowing;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryname;
//@property (strong, nonatomic) IBOutlet ASPVideoPlayer *videoPlayer;
//@property (strong, nonatomic) IBOutlet ASPVideoPlayer *videoPlayerBackgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoViewConstraint;

@end
