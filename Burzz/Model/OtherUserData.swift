//
//  OtherUserData.swift
//  Burzz
//
//  Created by Raul Menendez on 12/06/19.
//  Copyright © 2019 appsquad. All rights reserved.
//

import Foundation
public class OtherUserData {
    
    var id : String? = ""
    var user_id : String? = ""
    var headline : String? = ""
    var about_me : String? = ""
    var job : String? = ""
    var education : String? = ""
    var company : String? = ""
    var interest : String? = ""
    var looking_for : String? = ""
    var industry : String? = ""
    var education_level : String? = ""
    var experience : String? = ""
    var drinking : String? = ""
    var smoking : String? = ""
    var pets : String? = ""
    var kids : String? = ""
    var new_to_area : String? = ""
    var releationship : String? = ""
    var interest_in : String? = ""
    var likes : String? = ""
    var dislikes : String? = ""
    var exercise : String? = ""
    var religion : String? = ""
    var zodiac : String? = ""
    var height : String? = ""
    var name : String? = ""
    var profile_picture : String? = ""
    var gender : String? = ""
    var location : String? = ""
    var latitude : String? = ""
    var longitude : String? = ""
    var dob : String? = ""
    var image : Array<Any>?
    var preference : Array<Any>?
    var distance : String? = ""
    
    var job_data : Array<Any>?
    var education_data : Array<Any>?
    var insta_images : Array<Any>?
    
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let result_list = Result.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     - returns: Array of Result Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Result]
    {
        var models:[Result] = []
        for item in array
        {
            models.append(Result(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let result = Result(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Result Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        
        id = dictionary["id"] as? String ?? ""
        user_id = dictionary["user_id"] as? String ?? ""
        headline = dictionary["headline"] as? String ?? ""
        about_me = dictionary["about_me"] as? String ?? ""
        job = dictionary["job"] as? String ?? ""
        education = dictionary["education"] as? String ?? ""
        company = dictionary["company"] as? String ?? ""
        interest = dictionary["interest"] as? String ?? ""
        looking_for = dictionary["looking_for"] as? String ?? ""
        industry = dictionary["industry"] as? String ?? ""
        education_level = dictionary["education_level"] as? String ?? ""
        experience = dictionary["experience"] as? String ?? ""
        drinking = dictionary["drinking"] as? String ?? ""
        smoking = dictionary["smoking"] as? String ?? ""
        pets = dictionary["pets"] as? String ?? ""
        kids = dictionary["kids"] as? String ?? ""
        new_to_area = dictionary["new_to_area"] as? String ?? ""
        releationship = dictionary["releationship"] as? String ?? ""
        interest = dictionary["interest"] as? String ?? ""
        likes = dictionary["likes"] as? String ?? ""
        dislikes = dictionary["dislikes"] as? String ?? ""
        exercise = dictionary["exercise"] as? String ?? ""
        religion = dictionary["religion"] as? String ?? ""
        zodiac = dictionary["zodiac"] as? String ?? ""
        height = dictionary["height"] as? String ?? ""
        profile_picture = dictionary["profile_picture"] as? String ?? ""
        name = dictionary["name"] as? String ?? ""
        gender = dictionary["gender"] as? String ?? ""
        location = dictionary["location"] as? String ?? ""
        latitude = dictionary["latitude"] as? String ?? ""
        longitude = dictionary["longitude"] as? String ?? ""
        dob = dictionary["dob"] as? String ?? ""
        image = dictionary["images"] as? Array<Any>
        preference = dictionary["preference"] as? Array<Any>
        distance = dictionary["distance"] as? String ?? ""
        
        job_data = dictionary["job_data"] as? Array<Any>
        education_data = dictionary["education_data"] as? Array<Any>
        insta_images = dictionary["insta_images"] as? Array<Any>
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.user_id, forKey: "user_id")
        dictionary.setValue(self.headline, forKey: "headline")
        dictionary.setValue(self.about_me, forKey: "about_me")
        dictionary.setValue(self.job, forKey: "job")
        dictionary.setValue(self.education, forKey: "education")
        dictionary.setValue(self.company, forKey: "company")
        dictionary.setValue(self.interest, forKey: "interest")
        dictionary.setValue(self.looking_for, forKey: "looking_for")
        dictionary.setValue(self.industry, forKey: "industry")
        dictionary.setValue(self.education_level, forKey: "education_level")
        dictionary.setValue(self.experience , forKey: "experience")
        dictionary.setValue(self.drinking, forKey: "drinking")
        dictionary.setValue(self.smoking, forKey: "smoking")
        dictionary.setValue(self.pets, forKey: "pets")
        dictionary.setValue(self.kids , forKey: "kids")
        dictionary.setValue(self.new_to_area, forKey: "new_to_area")
        dictionary.setValue(self.releationship, forKey: "releationship")
        dictionary.setValue(self.interest, forKey: "interest")
        dictionary.setValue(self.likes , forKey: "likes")
        dictionary.setValue(self.dislikes, forKey: "dislikes")
        dictionary.setValue(self.exercise , forKey: "exercise")
        dictionary.setValue(self.religion, forKey: "religion")
        dictionary.setValue(self.zodiac , forKey: "zodiac")
        dictionary.setValue(self.height , forKey: "height")
        dictionary.setValue(self.profile_picture , forKey: "profile_picture")
        dictionary.setValue(self.name , forKey: "name")
        dictionary.setValue(self.gender , forKey: "gender")
        dictionary.setValue(self.location, forKey: "location")
        dictionary.setValue(self.latitude , forKey: "latitude")
        dictionary.setValue(self.longitude, forKey: "longitude")
        dictionary.setValue(self.dob, forKey: "dob")
        dictionary.setValue(self.image, forKey: "images")
        dictionary.setValue(self.image, forKey: "preference")
        dictionary.setValue(self.distance, forKey: "distance")
        dictionary.setValue(self.job_data, forKey: "job_data")
        dictionary.setValue(self.education_data, forKey: "education_data")
        dictionary.setValue(self.insta_images, forKey: "insta_images")
        
        return dictionary
        
    }
    
}


