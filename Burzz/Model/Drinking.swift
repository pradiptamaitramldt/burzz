


import Foundation
 

public class Drinking {
	public var id : String?
	public var title : String?
	public var status : String?


    public class func modelsFromDictionaryArray(array:NSArray) -> [Drinking]
    {
        var models:[Drinking] = []
        for item in array
        {
            models.append(Drinking(dictionary: item as! NSDictionary)!)
        }
        return models
    }


	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? String
		title = dictionary["title"] as? String
		status = dictionary["status"] as? String
	}

		

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.status, forKey: "status")

		return dictionary
	}

}
